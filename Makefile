MAJOR ?= 6
MINOR ?= 1
BUILD ?= 0
.PHONY: all

all: build push

build:
	docker build --rm -t fingerprintsoft/node .
	docker tag fingerprintsoft/node fingerprintsoft/node:${MAJOR}.${MINOR}.${BUILD}

push:
	docker push fingerprintsoft/node
	docker push fingerprintsoft/node:${MAJOR}.${MINOR}.${BUILD}
